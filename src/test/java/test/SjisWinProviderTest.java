package test;

import org.junit.Test;

import java.nio.charset.Charset;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class SjisWinProviderTest {

    @Test
    public void test() {
        Charset c = Charset.forName("sjis-win");

        assertThat(new String(new byte[] { (byte) 0x82, (byte) 0xA0 }, c), is("あ"));
    }

}
