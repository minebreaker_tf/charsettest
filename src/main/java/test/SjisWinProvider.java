package test;

import java.nio.charset.Charset;
import java.nio.charset.spi.CharsetProvider;
import java.util.Iterator;
import java.util.stream.Stream;

public final class SjisWinProvider extends CharsetProvider {

    private static final Charset set = Charset.forName("MS932");

    @Override
    public Iterator<Charset> charsets() {
        return Stream.of(set).iterator();
    }

    @Override
    public Charset charsetForName(String charsetName) {
        return charsetName.equals("sjis-win") ? set : null;
    }

}
